<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Chambre
 *
 * @ORM\Table(name="chambre")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ChambreRepository")
 */
class Chambre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="numchambre", type="integer")
     */
    private $numchambre;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numchambre
     *
     * @param integer $numchambre
     *
     * @return Chambre
     */
    public function setNumchambre($numchambre)
    {
        $this->numchambre = $numchambre;

        return $this;
    }

    /**
     * Get numchambre
     *
     * @return int
     */
    public function getNumchambre()
    {
        return $this->numchambre;
    }
}

