<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fiche
 *
 * @ORM\Table(name="fiche")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FicheRepository")
 */
class Fiche
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="dateCreation", type="string", length=255)
     */
    private $dateCreation;

    /**
     * @var string
     *
     * @ORM\Column(name="diagnostique", type="string", length=255)
     */
    private $diagnostique;


    /**
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Patient")
     * @ORM\JoinColumn(name="nomprenom",referencedColumnName="id",onDelete="CASCADE")
     */
    protected $nomprenom;

    /**
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DossierMedical")
     * @ORM\JoinColumn(name="idDossierMedi",referencedColumnName="id",onDelete="CASCADE")
     */
    protected $idDossierMedi;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreation
     *
     * @param string $dateCreation
     *
     * @return Fiche
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set diagnostique
     *
     * @param string $diagnostique
     *
     * @return Fiche
     */
    public function setDiagnostique($diagnostique)
    {
        $this->diagnostique = $diagnostique;

        return $this;
    }

    /**
     * Get diagnostique
     *
     * @return string
     */
    public function getDiagnostique()
    {
        return $this->diagnostique;
    }

    /**
     * @return mixed
     */
    public function getNomprenom()
    {
        return $this->nomprenom;
    }

    /**
     * @param mixed $nomprenom
     */
    public function setNomprenom($nomprenom)
    {
        $this->nomprenom = $nomprenom;
    }

    /**
     * @return mixed
     */
    public function getIdDossierMedi()
    {
        return $this->idDossierMedi;
    }

    /**
     * @param mixed $idDossierMedi
     */
    public function setIdDossierMedi($idDossierMedi)
    {
        $this->idDossierMedi = $idDossierMedi;
    }



}

