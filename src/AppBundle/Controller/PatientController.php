<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Location;
use AppBundle\Entity\Medecin;
use AppBundle\Entity\Modele;
use AppBundle\Entity\Product;
use AppBundle\Entity\Voiture;
use AppBundle\Form\LocationType;
use AppBundle\Form\MedecinType;
use AppBundle\Form\ModeleType;
use AppBundle\Form\VoitureType;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\VarDumper\Cloner\Data;

class PatientController extends Controller
{


    /**
     * @Route("/access", name="access")
     */
    public function accessAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('baselayout.html.twig');
    }

    /**
     *
     * @Route("create", name="create")
     */
    public function createAction(Request $request){
        return $this->render('medecin.html.twig');
    }

    /**
     * @Route("/createmedecin", name="createmedecin")
     */
    public function createmedecinAction(Request $request)
    {
        $medecin=new Medecin();
        $form=$this->createForm(MedecinType::class, $medecin);
        $formview =$form->createView();
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid())
        {
            $save=$this->getDoctrine()->getManager();
            $save->persist($medecin);
            $save->flush();
        }
        return $this->render('createpatient.html.twig');
    }

}
